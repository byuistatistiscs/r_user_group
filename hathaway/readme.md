# Hathaway Readme File

This is a good place to take notes on the scripts I have placed in my folder.  I can even link to the scripts here.

#### Links to My Pages

[Jupyter](pages/Using_jupyter.md)
    
[R packages of Note](pages/Intersting_Rpackages.md)
   
[Shiny Examples](pages/Shiny.md)
   
[I-Learn 3.0 Notes](pages/ILearn3.md)
   
[Nice Introductory R Material](pages/Rinfo.md)

#### Notes on Git 

http://stackoverflow.com/questions/9672319/gitignore-file-where-should-i-put-it-in-my-xcode-project


