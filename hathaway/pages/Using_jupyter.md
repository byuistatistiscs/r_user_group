# Jupyter (Interactive documents for R, Python, and Julia)

This interactive document workspace is much like Rstudio.  It works through Python but has a setup for R and Julia (Julia is a new language trying to be built up to compete with R and Python).  It has some benefits and disadvantages to Rstudio.  Fun to try, but have not put it to real use yet.

Here are some links that I have found as I have got my environment set up.

[Good clean explanation using R with Jupyter](https://www.continuum.io/blog/developer/jupyter-and-conda-r)

[Example of using R with Jypyter](http://statisfactions.com/2015/interactive-r-notebooks-with-jupyter-and-sagemathcloud/)
   
[Good Examples](https://github.com/ipython/ipython/wiki/A-gallery-of-interesting-IPython-Notebooks) --  These notebooks compile on Github. So it may be an easier way to share work than with Rstudio...
   
[Sage Math Cloud](https://cloud.sagemath.com/) -- This looks like a potential way to host files.
   
[Good YouTube Video on Sage Math Cloud](https://www.youtube.com/watch?v=YyEReiAYGlU)


