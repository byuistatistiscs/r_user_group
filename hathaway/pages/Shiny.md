* [Nice examples from Cal Poly](http://statistics.calpoly.edu/shiny#sampdist)
* [Nice examples from Duke](http://www2.stat.duke.edu/~mc301/shinyed/)
* CLT Examples
    - [ihstevenson](https://ihstevenson.shinyapps.io/sample_means/)
    - [galler example](https://gallery.shinyapps.io/dist_calc/)
    - [Cal Poly](http://shiny.stat.calpoly.edu/Sampling_Distribution/)
* [Distribution Calculator](https://gallery.shinyapps.io/dist_calc/) -- Like our Math 221 Applet
