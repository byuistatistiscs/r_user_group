---
title: "D2L or Brightspace or I-Learn 3.0"
author: "J. Hathaway"
date: "January 5, 2016"
output: html_document
---

### Introduction

As I transition to I-Learn 3.0, I thought it would be good to put up some notes on my transition.  I am only using I-Learn 3.0 for one course this semester (Math 221 Introductory Statistics).  I hope some of these notes are helpful to others.  At the least it will help me remember some GUI steps. I will make sections for each topic that I want to remember and hopefully put some screen shots in as well...

### Course Creation

The I-Learn support team is the only one that can create courses.  Call 1535 to get the class created.

### Using a Reference Course with TAs as Students

The I-Learn support students don't have permission (or faculty) to add students to the course.  We need to send a list of TAs to the I-Learn support team for their manager to enter them into the reference course.


### Making Final Grades Visible to Students



### Deleting a Course 

There are three steps to get the the point where you can set your course such that all the students can see there overall grade during the semester.

1. Click "My Grades" and then select "Grades" from the drop down menu.
   
![step1](../images/release1.png)
   
2. Then click on the drop down arrow next to "Final Calculated Grade" and select "Grade All".  This will not grade anything at this point just open up another screen for you to make changes.
   
![step2](../images/release2.png)

3. This will open the "Final Grades" view where you select the drop down arrow and select "Release All" which will open a pop-up warning window.  By checking ok in this pop-up window your students will then be able to see their overall grade during the course.
   
![step3](../images/release3.png)
   



### Notes from conversation with I-Learn 3.0 Transition Team

##### 1/5/2016 
* Discussed way to edit show, due, end dates.  It appears to require a lot of point and click.  There are some quicker ways.
* Also asked about appropriateness of editing XML and html files in the zipped folder.  He said it would work and that he was working on a tool that would do this as well.