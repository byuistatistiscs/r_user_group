## Good websites to get up to spead on R

[Nice Class on R](http://mgimond.github.io/ES218/index.html)

### Understanding the Grammar of Graphics 

Colby College has developed a nice course for an [introduction to R](http://mgimond.github.io/ES218/index.html).  In their fourth week they [cover ggplot2](http://mgimond.github.io/ES218/Week04c.html) which is a great package to make data access and interactivity from R usable.

ggplot2 has a unique scripting paradigm when compared to the base plotting functionality.  It took me a while to get over the hump when I started with it 5 or 6 years ago.  At that time, I was in the process of leaving base graphics for [lattice](http://mgimond.github.io/ES218/Week04b.html) because I wanted to make "prettier" visualizations and have more control of the plotting environment.  Lattice is a big jump from base plotting as well.  I was swayed be the ease of controlling the plotting page with ggplot2 but both felt like a much better option than base graphics.

One of the reasons I adopted ggplot2 was the ability to radically switch the graphic view without much change in the underlying code.

Here are two examples of plots from Bill Cleveland's [The Elements of Graphic Data](http://www.amazon.com/Elements-Graphing-Data-William-Cleveland/dp/0963488414).  In 1985 this book was very cutting edge.

Here are some pictures that show Bill discussion why we use different graphics.

#### Example 1
![Cleveland 1 ok](../images/Cleveland_1_ok.png)
![Cleveland 1 better](../images/Cleveland_1_better.png)


##### Example 2

![Cleveland 2 ok](../images/Cleveland_2_ok.png)
![Cleveland 2 better](../images/Cleveland_2_better.png)
   
* How hard do you think it would be to program the change in data displays from the OK image to the better image in the 80s?
* How hard would it be to change using current base graphics in R?
* In ggplot2 it is almost trivial (once you know the language)?


   
   