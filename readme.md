#  Readme file for the BYU-I R User Group

This Git repo will be a location where we can share files.  Anyone that has a script to present to the user group can push their files to the repo.  When at the meeting the rest of us can pull down the repo to get the latest files.

## Install SourceTree

[SourceTree](https://www.sourcetreeapp.com/) is a nice GUI for git.  It works on [Mac](https://downloads.atlassian.com/software/sourcetree/SourceTree_2.1.dmg) and [Windows](https://downloads.atlassian.com/software/sourcetree/windows/SourceTreeSetup_1.7.0.32509.exe).

## Introduction to R

[PDF on R and statistics](https://qualityandinnovation.files.wordpress.com/2015/04/radziwill_statisticseasierwithr_preview.pdf)

[Good Website for R and statistics resources](http://www.r-statistics.com/2009/10/free-statistics-e-books-for-download/)
   
[Nice Class on R](http://mgimond.github.io/ES218/index.html)
* [ggplot2 intro](http://mgimond.github.io/ES218/Week04c.html)
* 
   

## Member Readme Pages

[Brother Hathaway](hathaway/readme.md)
   
[Caleb](Caleb/readme.md)
